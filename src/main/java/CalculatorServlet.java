import util.ArithmeticCalculator;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "CalculatorServlet", urlPatterns = {"/calc/*"})
public class CalculatorServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String expression = (String) session.getAttribute("expression");
        HashMap<String,Integer> params = (HashMap<String, Integer>) session.getAttribute("params");
        for (Map.Entry<String,Integer> param: params.entrySet()) {
            expression = expression.replaceAll(param.getKey(),Integer.toString(param.getValue()));
        }

        if (expression.matches(".*[a-z].*")) {
            resp.setStatus(409);
            return;
        }

        ArithmeticCalculator arithmeticCalculator = new ArithmeticCalculator(expression,0);
        String result = arithmeticCalculator.process();

        PrintWriter printWriter = resp.getWriter();
        printWriter.print(result);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String url = req.getRequestURI();
        String param = url.replaceAll("/calc/","");
        String value = (String) req.getAttribute("value");


        if ("expression".equals(param)) {

            if (req.getSession().getAttribute("expression")!=null) {
                resp.setStatus(200);
            } else {
                resp.setStatus(201);
            }

            req.getSession().setAttribute("expression", value);
        } else {
            HttpSession session = req.getSession();
            HashMap<String,Integer> params = (HashMap<String, Integer>) session.getAttribute("params");
            if (params==null) {
                params = new HashMap<String,Integer>();
            }

            if (params.containsKey(param)) {
                resp.setStatus(200);
            } else {
                resp.setStatus(201);
            }

            if (params.containsKey(value)) {
                params.put(param,params.get(value));
            } else {
                int intValue = Integer.parseInt(value);
                params.put(param,intValue);
            }

            session.setAttribute("params", params);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String url = req.getRequestURI();
        String param = url.replaceAll("/calc/","");
        HttpSession session = req.getSession();
        HashMap<String,Integer> params = (HashMap<String, Integer>) session.getAttribute("params");
        if (params.containsKey(param)) {
            params.remove(param);
            resp.setStatus(204);
        }
    }
}
