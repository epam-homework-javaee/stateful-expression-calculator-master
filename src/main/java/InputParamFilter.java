import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;

@WebFilter(urlPatterns = "/calc/*")
public class InputParamFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
       HttpServletRequest httpServletRequest = (HttpServletRequest)servletRequest;
       HttpServletResponse httpServletResponse = (HttpServletResponse)servletResponse;

       if ("PUT".equals(httpServletRequest.getMethod())) {
            String url = httpServletRequest.getRequestURI();
            String param = url.replaceAll("/calc/","");
            BufferedReader br = httpServletRequest.getReader();
            String value = br.lines().findFirst().orElse("");
            httpServletRequest.setAttribute("value",value);

            if (value.contains("bad format")) {
                httpServletResponse.setStatus(400);
               return;
           }

           if (!"expression".equals(param) && value.matches("-?\\d+")) {
               int intValue = Integer.parseInt(value);
               if (intValue<-10000 || intValue>10000) {
                   httpServletResponse.setStatus(403);
                   return;
               }
           }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
